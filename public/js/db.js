// Offline data
var database = firebase.firestore();

database.enablePersistence()
    .catch(function (err) {
        if (err.code == 'failed-precondition') {
            // probably multiple tabs open at once
        } else if (err.code == 'unimplemented') {
            // lack of browser support
            console.log('Presestence is not available');
        }
    });

database.collection('product-list').onSnapshot(function (snapshot) {
    // console.log(snapshot.docChanges());
    snapshot.docChanges().forEach(function (change) {
        if (change.type === 'added') {
            // Added the document data to the we page
            renderProductItems(change.doc.data(), change.doc.id);
        }
        if (change.type === 'removed') {
            // Added the document data from the we page
            removeItem(change.doc.id);
        }
    });
});


// Add new car 

const form = document.getElementById('form');

if (form) {
    form.addEventListener('submit', function (evt) {
        evt.preventDefault();
        const items = {
            title: form.title.value,
            size: form.size.value,
            color: form.color.value,
            brand: form.brand.value,
            qty: form.qty.value,
            price: form.price.value
        };
        database.collection('product-list').add(items)
            .catch(function (err) {
                console.log("Error", err);
            });
        form.reset();
    });
}

// deleting car 

const items = document.getElementById('itemListContainer');
if (items) {

    items.addEventListener('click', function (evt) {
        evt.preventDefault();
        if (evt.target.tagName === 'BUTTON' || evt.target.tagName === 'I') {
            const id = evt.target.getAttribute('data-id');
            database.collection('product-list').doc(id).delete();
        }
    });

}