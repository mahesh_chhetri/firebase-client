const itemListContainer = document.getElementById('itemListContainer');

const renderProductItems = function (data, id) {
    const html = `
    <tr data-id="${id}">
        <th>${data.title}</th>
        <td>${data.size}</td>
        <td>${data.color}</td>
        <td>${data.brand}</td>
        <td>${data.qty}</td>
        <td>${data.price}</td>
        <td class="action"><button data-id="${id}" class="btn btn-danger"><i  data-id="${id}" class="fa fa-remove"></i></button></td>
    </tr>
    `;

    if (itemListContainer) {
        itemListContainer.innerHTML += html;
    }
};


const removeItem = function (id) {
    const carItem = document.querySelector(`tr[data-id=${id}]`);
    carItem.remove();
}